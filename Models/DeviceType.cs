using System;


namespace ASP.Models
{
        public class DeviceType
        {
            public enum deviceType { 
                presenceSensor = 0, 
                temperatureSensor = 1, 
                brightnessSensor = 2, 
                atmosphericPressureSensor= 3, 
                humiditySensor= 4, 
                soundLevelSensor= 5, 
                gpsSensor= 6, 
                co2Sensor= 7, 
                ledDevice= 8,
                beeperDevice= 9
                };    
        }
}