using Microsoft.EntityFrameworkCore;

namespace ASP.Models
{
    public class TelemetryContext : DbContext
    {
        public TelemetryContext(DbContextOptions<TelemetryContext> options)
            : base(options)
        {
        }

        public DbSet<Telemetry> Telemetries { get; set; }
    }
}