using System;
using System.ComponentModel.DataAnnotations;

namespace ASP.Models
{

    //  public enum DeviceType { Sun, Mon, Tue, Wed, Thu, Fri, Sat };
    public class Telemetry
    {
        public Guid id { get; set; }
        public DateTime? metricDate { get; set; }
        public string metricValue { get; set; }
        public Guid deviceId { get; set; }
        // public Device deviceId {get; set; }

        public string deviceType { get; set; }


    }
}