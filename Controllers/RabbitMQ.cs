using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace ASP.Controllers
{
    class RabbitMQ
    {
        public void subscribe(String host, String channelChoice, String type)
        {
            var factory = new ConnectionFactory() { HostName = host };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                ////
                channel.ExchangeDeclare(exchange: channelChoice, type: type);
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(
                    queue: queueName,
                    exchange: channelChoice,
                    routingKey: ""
                );
                Console.WriteLine(" [*] Waiting for messages.");
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                };
                string consumerTag = channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

                // Console.WriteLine("testest");
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        public void publish(String host, String channelChoice, String type)
        {
            var factory = new ConnectionFactory() { HostName = host };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                ////
                channel.ExchangeDeclare(exchange: channelChoice, type: type);
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(
                    queue: queueName,
                    exchange: channelChoice,
                    routingKey: ""
                );
                Console.WriteLine(" [*] Waiting for messages.");
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                };
                string consumerTag = channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

                // Console.WriteLine("testest");
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
