using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASP.Models;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;



namespace ASP.Controllers
{
    [Route("device")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly DeviceContext _context;

        public DeviceController(DeviceContext context)
        {
            _context = context;

            // if (_context.Devices.Count() == 0)
            // {
            //     // Create a new TodoItem if collection is empty,
            //     // which means you can't delete all TodoItems.
            //     _context.Devices.Add(new Device { test = "Item1" });
            //     _context.SaveChanges();
            // }
        }
        // GET: /device
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Device>>> GetDevices()
        {
            return await _context.Devices.ToListAsync();
        }

        // GET: device/5
        [HttpGet("/device/{id}")]
        public async Task<ActionResult<Device>> GetDevice(Guid id)
        {
            var device = await _context.Devices.FindAsync(id);

            if (device == null)
            {
                return NotFound();
            }

            return device;
        }

        // POST: api/Todo
        [HttpPost()]
        public async Task<ActionResult<Device>> PostDevice(Device device)
        {
            _context.Devices.Add(device);
            // Console.WriteLine(device);
            string json = JsonConvert.SerializeObject(device);
            await _context.SaveChangesAsync();
            var factory = new ConnectionFactory() { HostName = "broker.atlantis.com" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "devices", type: "direct");

                var message = GetMessage(json);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "devices", routingKey: "", basicProperties: null, body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }

            // return CreatedAtAction(nameof(GetDevice), new { id = device.id }, device);
            // return Created();
            // return new HttpStatusCodeResult(HttpStatusCode.OK);
            return StatusCode(200);
        }
        private static string GetMessage(string args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World!");
        }

        [HttpPost("/device/{id}/telemetry")]
        public async Task<ActionResult<Device>> PostDeviceTelemetry(Telemetry telemetry, Guid id)
        {
            // _context.Devices.Add(device);
            // string json = JsonConvert.SerializeObject(device);
            // telemetry.MetricDate = DateTime.UtcNow;
            // telemetry.MetricDate = DateTime.Now;
            telemetry.id = Guid.NewGuid();
            telemetry.deviceId = id;
            string json2 = JsonConvert.SerializeObject(telemetry);
            await _context.SaveChangesAsync();
            var factory = new ConnectionFactory() { HostName = "broker.atlantis.com" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "metrics", type: "direct");

                // var message = GetMessage(json);
                var message = GetMessage(json2);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "metrics", routingKey: "", basicProperties: null, body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
            // return "200"
            // return CreatedAtAction(nameof(GetDevice), new { id = device.id }, device);
            // return CreatedAtAction(nameof(GetDevice), new { id = new Guid() });
            // return CreatedAtRoute(
            //     actionName: "GetDevice",
            //     routeValues: new { id = page.PageId },
            //     value: page);
            // }
            return Ok(); 
        }
    }
}
