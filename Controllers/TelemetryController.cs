using System;
// using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASP.Models;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;



namespace ASP.Controllers
{
    [Route("telemetries")]
    [ApiController]
    public class TelemetryController : ControllerBase
    {
        private readonly TelemetryContext _context;

        public TelemetryController(TelemetryContext context)
        {
            _context = context;

            if (_context.Telemetries.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                // _context.Telemetries.Add(new Telemetry { metricValue = "string" });
                // _context.SaveChanges();
            }
        }
        // GET: /device
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Telemetry>>> GetTelemetries()
        {
            return await _context.Telemetries.ToListAsync();
        }
    }
}
