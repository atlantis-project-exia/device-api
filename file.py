import requests
import random
import datetime
import uuid
from uuid import UUID
import struct
import json

uuid_list = ["261df9ad-a542-4760-9da7-312cab868319", "f12193ad-6b3c-4d71-bac3-e862df22e4ad", "fa477604-c1d3-41f9-8256-ef4582364ab0"]

headers = {
    'Content-type': 'application/json',
}

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)

for i in range(1,100) :
    data = {"Id": str(uuid.uuid4()), "DeviceId": str(uuid.uuid4()), "Value": bytearray(struct.pack("f", random.uniform(0, 30))).decode('latin-1').replace("'", '"'), "CreatedAt": str(datetime.datetime.now()), "MetricTypeId": random.choice(uuid_list)}
    response = requests.post('https://localhost:5001/device', headers=headers, verify=False,json=data)
    print(response)
    print(data)